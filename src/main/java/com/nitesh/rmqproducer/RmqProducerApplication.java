package com.nitesh.rmqproducer;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nitesh.rmqproducer.direct.PictureProducer;
import com.nitesh.rmqproducer.entity.Employee;
import com.nitesh.rmqproducer.entity.Picture;
import com.nitesh.rmqproducer.fanout.HRProuducer;

@SpringBootApplication
//@EnableScheduling
public class RmqProducerApplication implements CommandLineRunner {

//	@Autowired
//	private RabbitProducer rabbitProducer;
//	@Autowired
//	private FixedRateProducer fixrateProducer;
//	@Autowired
//	private EmployeeJsonProducer empProducer;

	@Autowired
	private HRProuducer hrProuducer;
	
	@Autowired
	private PictureProducer pictureProducer;

	public static void main(String[] args) {
		SpringApplication.run(RmqProducerApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
//		publishMessageInFanOutExchange(10);
		publishMessageInDirectExchange(10);
	}
	
	private void publishMessageInFanOutExchange(int limit) {
		for (int i = 0; i < limit; i++) {
			Employee emp = new Employee("emp: " + i, "nitesh: " + i, "Dumka");
			System.out.println(emp);
			hrProuducer.publish(emp);
		}
	}

	private void publishMessageInDirectExchange(int limit) {

		List<String> SOURCES = List.of("mobile", "web");
		List<String> TYPES = List.of("jpg", "png", "svg");
		for (int i = 0; i < limit; i++) {
			Picture picuture = new Picture();
			picuture.setName("Picture " + i);
			picuture.setSize(ThreadLocalRandom.current().nextLong(1, 10001));
			picuture.setSource(SOURCES.get(i % SOURCES.size()));
			picuture.setType(TYPES.get(i % TYPES.size()));
			
			try {
				pictureProducer.sendMessage(picuture);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

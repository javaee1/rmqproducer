package com.nitesh.rmqproducer.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nitesh.rmqproducer.entity.Employee;

@Service
public class EmployeeJsonProducer {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private ObjectMapper objMapper;
	
	public void sendMessage(Employee emp) {
		
		try {
			var json = objMapper.writeValueAsString(emp);
			System.out.println(json);
			rabbitTemplate.convertAndSend("course.employee", json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
//	public static void main(String [] args) {
//		ObjectMapper objMapper = new ObjectMapper();
//		Employee emp = new Employee("1", "nitesh", null);
//		
//		try {
//			var json = objMapper.writeValueAsString(emp);
//			
//			System.out.println(json);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}

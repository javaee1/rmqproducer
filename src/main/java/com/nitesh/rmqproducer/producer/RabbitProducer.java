package com.nitesh.rmqproducer.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


//@Service
public class RabbitProducer {
	
//	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	public void sendHello(String name) {
		rabbitTemplate.convertAndSend("TestQueue", "Hello "+ name);
		rabbitTemplate.convertAndSend("helloworld", "Hello "+ name);
	}

}

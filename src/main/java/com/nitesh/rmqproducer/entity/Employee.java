package com.nitesh.rmqproducer.entity;

public class Employee {

//	@JsonProperty("employee_id")
	private String id;
	private String name;

//	@JsonProperty("birth_date")
//	@JsonSerialize(using = CustomLocalDateSerializer.class)
	private String city;

	public Employee(String id, String name, String city) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", city=" + city + "]";
	}

}

package com.nitesh.rmqproducer.fanout;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nitesh.rmqproducer.entity.Employee;

@Service
public class HRProuducer {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	public void publish(Employee emp) {
		try {
			var json = objectMapper.writeValueAsString(emp);
			rabbitTemplate.convertAndSend("x.hr", "", json);			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

}
